import { AppHeroesPage } from './app.po';

describe('app-heroes App', () => {
  let page: AppHeroesPage;

  beforeEach(() => {
    page = new AppHeroesPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
